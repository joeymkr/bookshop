README

supplied is a prebuilt jar of the BookStore application. This can be found in BookShop_jar.

source files and test classes are in src.

To run the test classes I used maven, with a dependency for JUnit. The full project can be access online at

    - https://bitbucket.org/joeymkr/bookshop

The jar supplied is not a uberjar and does not contain the JUnit jar. To install via maven cd to BookShop
and type the following:

- mvn clean install

- mvn test



NOTE: The precision of the currency calculation is not fully accurate because of the precision of doubles.
It is known that for true precision when working with currency BigDecimal should be used.