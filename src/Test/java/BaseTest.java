import Business.OrderManager;
import org.junit.*;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
/**
 * Created by joe roberts on 04/06/2015.
 * tests bookstore application
 */
public class BaseTest {
    Map<Long, Integer> items;
    OrderManager manager;
    DecimalFormat df;
    @Before
    public void createInjectionParams(){
        manager = new OrderManager();
        df = new DecimalFormat("####0.00");
    }

    @Test
    public void testingTotalCost(){
        items = new HashMap<Long, Integer>();
        items.put(1l, 2);

        String format = df.format(manager.manageOrder(items));
        double result = Double.parseDouble(format);
        assertEquals(28.88, result, 0.00);
    }

    @Test
    public void test2BooksPublishDiscount(){
        items = new HashMap<Long, Integer>();
        items.put(2l, 1);
        items.put(5l, 1);

        String format = df.format(manager.manageOrder(items));
        double result = Double.parseDouble(format);
        assertEquals(24.69, result, 0.001);
    }

    @Test
    public void test3BooksOver30Discount(){
        items = new HashMap<Long, Integer>();
        items.put(3l, 1);
        items.put(5l, 1);
        items.put(11l, 1);

        String format = df.format(manager.manageOrder(items));
        double result = Double.parseDouble(format);
        assertEquals(35.27, result, 0.001);
    }

    @Test
    public void test3BooksBothDiscounts(){
        items = new HashMap<Long, Integer>();
        items.put(2l, 1);
        items.put(5l, 1);
        items.put(11l, 1);

        String format = df.format(manager.manageOrder(items));
        double result = Double.parseDouble(format);
        assertEquals(36.01, result, 0.001);
    }
}
