import view.StoreView;

/**
 * Created by joe roberts on 04/06/2015.
 */
public class BookStoreRunner {

    public static void main(String[] args){
        StoreView view = new StoreView();
        view.display();
    }
}
