package Business;

import dao.BookDao;
import dao.BookDaoMockImpl;
import model.*;

import promos.*;
import service.BookServiceImpl;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by joe roberts on 29/05/2015.
 * ordermanager is interface to calculation system. requires a map of
 * book ids and quantities injected into manageOrder method which returns
 * the total transaction value as a double
 */
public class OrderManager {

    private BookDao db;
    private PromoHandler currentPromos;

    public OrderManager (){
        currentPromos = new PromoHandler();
    }


    /**handles an order from bookstore. requires a Map<Long, Integer>
    *  that represents book id in book database and quantity of individual book
    */
    public double manageOrder(Map<Long, Integer> books){

        //generates an order from list of book ids and quantities
        Order order = createOrder(books);

        Map items = order.getItems();
        Iterator entries = items.entrySet().iterator();

        //iterates through each Map entry to apply and check promotions
        while(entries.hasNext()){
            Map.Entry entry = (Map.Entry)entries.next();
            Book book = (Book)entry.getKey();

            iteratePromos(book);

            //adjusts total price of order whether discount has been applied or not
            double adjustTotal = order.getFinalCost() + book.getPrice();
            order.setFinalCost(adjustTotal);
        }

        //applies promotions based on entire order
        iteratePromos(order);

        return order.getFinalCost();
    }

    private Order createOrder(Map<Long, Integer> books){
        db = new BookDaoMockImpl();
        Order or = new Order();

        for (Map.Entry<Long, Integer> entry : books.entrySet()){
            or.addBook(db.getBook(entry.getKey()), entry.getValue());
        }

        return or;
    }

    private void iteratePromos(Object ob){
        Set<Promotion> promos = currentPromos.getCurrentPromos();

        for (Promotion promo : promos){
            promo.checkAndApply(ob);
        }
    }
}
