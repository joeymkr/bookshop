package Business;

import promos.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joe roberts on 29/05/2015.
 * acts as a mock promotion generator
 * creates two promotions total promo which discounts entire order if over £30.00
 * and publication promo which discounts individual books if published after 2000
 */
public class PromoHandler {
    private Set<Promotion> promos;

    public PromoHandler(){
        promos = new HashSet<Promotion>();
    }

    public Set<Promotion> getCurrentPromos(){
        promos.add(new TotalPromo());
        promos.add(new PublishingPromo());

        return promos;
    }
}
