package dao;

import model.Book;

import java.util.List;

public interface BookDao {

    Book getBook(Long id);

    List<Book> getBooks();
}
