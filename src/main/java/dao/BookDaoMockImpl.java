package dao;

import model.Book;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * acts as a mock database for books
 */
public class BookDaoMockImpl implements BookDao {

    private Map<Long, Book> mockBookMap = new HashMap<Long, Book>();

    public BookDaoMockImpl() {
        Book mockBook1 = new Book();
        mockBook1.setTitle("Moby Dick");
        mockBook1.setId(1L);
        mockBook1.setPrice(15.20);
        mockBook1.setYear(1851);
        mockBookMap.put(mockBook1.getId(), mockBook1);

        Book mockBook2 = new Book();
        mockBook2.setTitle("the terrible privacy of maxwell sim");
        mockBook2.setId(2L);
        mockBook2.setPrice(13.14);
        mockBook2.setYear(2010);
        mockBookMap.put(mockBook2.getId(), mockBook2);

        Book mockBook3 = new Book();
        mockBook3.setTitle("still life with woodpecker");
        mockBook3.setId(3L);
        mockBook3.setPrice(11.05);
        mockBook3.setYear(1980);
        mockBookMap.put(mockBook3.getId(), mockBook3);

        Book mockBook4 = new Book();
        mockBook4.setTitle("sleeping murder");
        mockBook4.setId(4L);
        mockBook4.setPrice(10.24);
        mockBook4.setYear(1976);
        mockBookMap.put(mockBook4.getId(), mockBook4);

        Book mockBook5 = new Book();
        mockBook5.setTitle("three men in a boat");
        mockBook5.setId(5L);
        mockBook5.setPrice(12.87);
        mockBook5.setYear(1889);
        mockBookMap.put(mockBook5.getId(), mockBook5);

        Book mockBook6 = new Book();
        mockBook6.setTitle("the time machine");
        mockBook6.setId(6L);
        mockBook6.setPrice(10.43);
        mockBook6.setYear(1895);
        mockBookMap.put(mockBook6.getId(), mockBook6);

        Book mockBook7 = new Book();
        mockBook7.setTitle("the caves of steel");
        mockBook7.setId(7L);
        mockBook7.setPrice(8.12);
        mockBook7.setYear(1954);
        mockBookMap.put(mockBook7.getId(), mockBook7);

        Book mockBook8 = new Book();
        mockBook8.setTitle("idle thoughts of an idle fellow");
        mockBook8.setId(8L);
        mockBook8.setPrice(7.32);
        mockBook8.setYear(1886);
        mockBookMap.put(mockBook8.getId(), mockBook8);

        Book mockBook9 = new Book();
        mockBook9.setTitle("a christmas carol");
        mockBook9.setId(9L);
        mockBook9.setPrice(4.32);
        mockBook9.setYear(1843);
        mockBookMap.put(mockBook9.getId(), mockBook9);

        Book mockBook10 = new Book();
        mockBook10.setTitle("a tale of two cities");
        mockBook10.setId(10L);
        mockBook10.setPrice(6.32);
        mockBook10.setYear(1859);
        mockBookMap.put(mockBook10.getId(), mockBook10);

        Book mockBook11 = new Book();
        mockBook11.setTitle("great expectations");
        mockBook11.setId(11L);
        mockBook11.setPrice(13.21);
        mockBook11.setYear(1861);
        mockBookMap.put(mockBook11.getId(), mockBook11);
    }

    @Override
    public Book getBook(Long id) {
        return mockBookMap.get(id);
    }

    @Override
    public List<Book> getBooks() {
        return new ArrayList<Book>(mockBookMap.values());
    }
}
