package model;

/**
 * Created by joe roberts on 29/05/2015.
 */
public class Book implements Comparable{

    private String title;
    private double price;
    private int year;
    private Long id;

    public Book (){

    }
    public Book (String t, double p, int y){
        title = t;
        price = p;
        year = y;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Long getId() {return id;}

    public void setId(Long id){ this.id = id;}

    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }

        if (this == o){
            return true;
        }

        return id != null && id.equals(((Book)o).getId());
    }

    @Override
    public int hashCode(){
        if (id != null){
            return id.hashCode();
        } else {
            return -1;
        }
    }

    @Override
    public int compareTo(Object o) {
        return this.id.compareTo(((Book)o).getId());
    }
}
