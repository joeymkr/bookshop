package model;

import java.util.Map;
import java.util.HashMap;
/**
 * Created by joe roberts on 29/05/2015.
 */
public class Order {

    private Map<Book, Integer> items = new HashMap<Book, Integer>();
    private double finalCost;

    public void addBook(Book book, int quantity){
        if (quantity >0 ) {
            if (items.containsKey(book)){
                quantity = items.get(book) + quantity;
            }
            items.put(book, quantity);
        }
    }

    public void removeBook(Book book, int quantity) {
        if (items.containsKey(book)) {
            int currentQuantity = items.get(book);

            if (currentQuantity > 1) {
                items.put(book, currentQuantity - 1);
            } else {
                items.remove(book);
            }
        }
    }

    public Map<Book, Integer> getItems(){
        return items;
    }

    public void setFinalCost(double cost){
        finalCost = cost;
    }

    public double getFinalCost(){return finalCost;}
}
