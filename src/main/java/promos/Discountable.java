package promos;

/**
 * Created by joe roberts on 29/05/2015.
 */
public interface Discountable {
    public boolean checkAndApply(Object ob);

}
