package promos;

/**
 * Created by joe roberts on 29/05/2015.
 *
 */
public abstract class Promotion implements Comparable<Promotion>, Discountable {

    //Type of promotion can either be for single unit (i.e. book) or group (i.e. whole transaction)
    protected enum PromotionTypes{
        SINGLE,
        GROUP
    }

    protected PromotionTypes promoType;

    public PromotionTypes getPromoType(){
        return promoType;
    }

    //compares the promotion enum type of promos.Promotion objects to sort.
    //The reason behind sorting the promos is because group discounts must be applied to whole transaction
    //after all single discounts have been applied.
    @Override
    public int compareTo(Promotion promo) {
        return promoType.compareTo(promo.getPromoType());
    }
}
