package promos;

import model.Book;

/**
 * Created by joe roberts on 29/05/2015.
 * applys promotion on single book if publication date is after 2000
 */
public class PublishingPromo extends Promotion {

    public PublishingPromo (){
        super.promoType = PromotionTypes.SINGLE;
    }
    @Override
    public boolean checkAndApply(Object ob){

        double price = 0.00;

        //check if instance is of book
        if (ob instanceof Book) {
            //get book price
            Book book = (Book) ob;
            price = book.getPrice();
            int publicationYear = book.getYear();

            if (publicationYear >= 2000) {
                double tenPercent = ((price * 10.00) / 100.00);

                book.setPrice(price - tenPercent);
                return true;
            }
        }

        return false;
    }
}
