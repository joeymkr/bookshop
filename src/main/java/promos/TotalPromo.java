package promos;

import model.Order;
import service.BookServiceImpl;

/**
 * Created by joe roberts on 29/05/2015.
 * applies discount on entire order if order is over £30.00
 */
public class TotalPromo extends Promotion {

    public TotalPromo(){
        super.promoType = PromotionTypes.GROUP;
    }
    @Override
    public boolean checkAndApply(Object ob){
        double price = 0.0;

        //check if instance or order
        if (ob instanceof Order){
            //get order price
            BookServiceImpl service = new BookServiceImpl();
            price = service.getOrderTotal((Order) ob);

            if (price >= 30.00) {
                //calculate discount
                double fivePercent = ((price * 5.00) / 100.00);

                //set order total
                ((Order) ob).setFinalCost((price - fivePercent));
                return true;
            }
        }

        return false;
    }
}
