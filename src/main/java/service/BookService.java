package service;

import model.Order;

public interface BookService {

    Double getOrderTotal(Order order);
}
