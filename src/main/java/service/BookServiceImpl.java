package service;

import model.Book;
import model.Order;

import java.util.Map;

/**
 * provides getOrderTotal method to calulate cost of whole basket (HashMap<Book, Integer>)
 * method returns double.
 */
public class BookServiceImpl implements BookService {

    @Override
    public Double getOrderTotal(Order order) {
        Double total = 0.0;

        for (Map.Entry<Book, Integer> entry : order.getItems().entrySet()) {
            Book book = entry.getKey();
            Integer quantity = entry.getValue();

            total += book.getPrice() * quantity;
        }

        return total;
    }
}
