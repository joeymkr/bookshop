package view;

import Business.OrderManager;
import dao.BookDao;
import dao.BookDaoMockImpl;
import model.Book;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;

/**
 * Created by joe roberts on 04/06/2015.
 * basic us for book store
 */
public class StoreView extends JFrame implements ActionListener {
    private BookDao db;
    private JLabel[] titles;
    private JLabel[] prices;
    private JRadioButton[] selectors;
    private JPanel panel;
    private JButton checkout;
    private JTextField total;
    private JTextField[] quantities;
    private Long[] ids;
    private DecimalFormat df;

    public StoreView(){
        db = new BookDaoMockImpl();
        df = new DecimalFormat("####0.00");
    }

    public void display(){
        Container contentPane = getContentPane();
        FlowLayout layout = new FlowLayout();
        layout.setAlignment(FlowLayout.LEADING);
        contentPane.setLayout(layout);


        panel = new JPanel();
        loadBooks();
        panel.setPreferredSize(new Dimension(400, 400));
        panel.setLayout(layout);

        checkout = new JButton("Checkout");
        checkout.setActionCommand("checkout");
        checkout.setPreferredSize(new Dimension(200, 25));
        checkout.addActionListener(this);

        contentPane.add(panel);
        contentPane.add(checkout);

        setTitle("Book Store");
        setSize(500,500);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void loadBooks(){
        //get books from db
        List<Book> bookList = db.getBooks();
        //get size of list for iteration and initiate JComponent arrays
        int size = bookList.size();
        titles = new JLabel[size];
        prices = new JLabel[size];
        selectors = new JRadioButton[size];
        ids = new Long[size];
        quantities = new JTextField[size];

        //iterate through each book and initialise JComponents for display
        for (int i = 0; i < size; i++){
            Book book = bookList.get(i);
            selectors[i] = new JRadioButton();
            titles[i] = new JLabel(book.getTitle());
            titles[i].setPreferredSize(new Dimension(200, 25));
            prices[i] = new JLabel("£" + df.format(book.getPrice()));
            prices[i].setPreferredSize(new Dimension(50,25));
            quantities[i] = new JTextField("1");
            quantities[i].setPreferredSize(new Dimension(50,25));
            panel.add(titles[i]);
            panel.add(prices[i]);
            panel.add(selectors[i]);
            panel.add(quantities[i]);
            //used to get book once selected
            ids[i] = book.getId();
        }
        JLabel totalCost = new JLabel("Total: ");
        totalCost.setPreferredSize(new Dimension(200, 25));
        total = new JTextField();
        total.setPreferredSize(new Dimension(50,25));
        total.setEditable(false);
        panel.add(totalCost);
        panel.add(total);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String event = e.getActionCommand();

        if (event.equalsIgnoreCase("checkout")){
            Map<Long, Integer> order = new HashMap<Long, Integer>();

            for (int i = 0; i < selectors.length; i++){
                if (selectors[i].isSelected()){
                    Integer quantity = tryParse(quantities[i].getText());
                    if (quantity == null){
                        JOptionPane.showMessageDialog(panel, "Quantity must be a number!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    order.put(ids[i],quantity);
                }
            }

            OrderManager orMan = new OrderManager();
            String finalCost = df.format(orMan.manageOrder(order));

            total.setText("£"+ finalCost);
        }
    }

    private Integer tryParse(String text){
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException nfe){
            return null;
        }
    }
}
